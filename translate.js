export const messages = {
  hu: {
    footer: 'ELTE, Térképtudományi és Geoinformatikai Intézet',
    header: {
      title: 'Multimédia a térképeken',
      tooltip_theme: 'Témaválasztó',
    },
    home: {
      title: 'Kezdőlap',
      introduction: `<h3 style="text-align: left">Üdvözöllek a honlapon!</h3>
                    Ez a weboldal egy térképészettel és geoinformatikával foglalkozó "weboldal sorozatnak" a része. Ez a rész a térképekre rakható multimédiás tartalmakkal foglalkozik, míg a sorozat másik két része a 
                    <a href="http://lazarus.elte.hu/hun/dolgozo/jesus/tt/kezdes.htm" target="_blank" aria-label="external link on new tab">térképtörténet <i class="fas fa-external-link-alt fa-sm"></i></a>
                    és a 
                    <a href="http://lazarus.elte.hu/hun/dolgozo/jesus/terinfo/kezdes.htm" target="_blank" aria-label="external link on new tab">térinformatika <i class="fas fa-external-link-alt fa-sm"></i></a>
                    témakörét mutatja be. Az eredeti honlapok 2006 körül készültek, és ebből kettő, köztük ez a honlap is szakdolgozat részeként került modernizálásra 2021-ben.
                    <br>
                    <br>
                    A weboldal elsősorban középiskolások számára készült, igyekszik a témát tömören, érthetően és érdekesen összefoglalni. A honlap tartalma 8 nagy fejezetre, azokon belül pedig további kisebb témákra van osztva. Szó van többek között az adatvizualizációról, a webes térképészetről és a kiterjesztett valóságról.`
    },
    navbar: {
      home: 'Kezdőlap',
      content: 'Tartalom',
      contact: 'Kapcsolat'
    },
    sidenav: {
      data: 'Az adatok ábrázolása',
      first_thematic_maps: 'Első tematikus térképek',
      modern_thematic_maps: 'Mai tematikus térképek',
      traditional_multimedia: 'Hagyományos multimédia a térképeken',
      digital_multimedia: 'Digitális multimédia a térképeken',
      web_cartography: 'Multimédia a webkartográfiában',
      mapping_services: 'Webes térképészeti szolgáltatások',
      modern_technologies: 'Modern technológiák',
    },
    data: {
      title: 'Az adatok ábrázolása',
      catalan: `<h3>Adatvizualizáció</h3>
                <p>Korai idők óta a tudósok igyekeztek az általuk végzett megfigyeléseket nemcsak adatokkal, képletekkel vagy leírásokkal lejegyezni, hanem kutatásaik eredményeit grafikailag is rögzítették, ezt nevezik ma adatvizualizációnak. 
                Ennek egyik legszebb példája az 1375-ben Abraham Cresques által készített <i>Katalán Atlasz</i>. Ebben olyan, a térképektől függetlenül készített diagramokat találunk, amelyek szemléletesen bemutatják többek között az Univerzummal vagy az ár-apály jelenségével kapcsolatos korabeli elképzeléseket.</p>`,
      graunt: `<h3>Korai adatbázisok</h3>
                <p>1662-ben született meg a mai statisztikai évkönyvek illetve digitális adatbázisok legkorábbi elődje. Ebben az évben John Graunt (Anglia, 1620-1674) halálozási adatokat gyűjtött London lakosságáról. Ezeket az adatokat témakörökbe és táblázatokba rendezte és kiadta egyetlen kötetben. A kutatók számára ma is nagyon értékesek ezek az adatok: elemzésük alapján többek között az is kiderült, hogy a 17. század második felében Londonban az átlagéletkor 27 év volt, és a város lakosságának a 65%-a 16 éves kor előtt meghalt.
                </p>`,
      playfair:`<h3>Grafikus megjelenítés új módszerei</h3>
                <p>John Graunt után a statisztikai adatok összegyűjtése más gazdasági területekre is kiterjedt. Lassan felvetődött az az igény, hogy ezeket az adatokat illetve a köztük levő összefüggéseket könnyebben áttekinthetővé tegyék. Ennek megvalósítására újra grafikai eszközökhöz folyamodtak: 1786-ban egy skót mérnök, William Playfair (1759-1823) elsőként használt oszlop-, kör- és vonaldiagramokat a gazdasági adatok bemutatására.
                </p>`,
      humboldt: `<h3>Alexander von Humboldt</h3>
                <p>Az adatok „diagramszerű” megjelenítése egyre népszerűbbé vált a természettudományokban is. Ebben számos feladatot vállalt a 19. század első felében a német Alexander von Humboldt, aki a természettudományok több ágában jelentős eredményeket ért el. Ő ábrázolta először diagram segítségével a hőmérséklet változását a földrajzi szélesség és hosszúság függvényében, de más szakterületeken (például Mexikóban a lakosság és a területek nagyságának az összehasonlítására) is szemléletesen alkalmazta a grafikai megjelenítést.
                  </p>`,
      pictorial: `<h3>Diagramok népszerűsítése</h3>
                  <p>A 19. században mindennapossá vált a diagramok alkalmazása a tudományos eredmények bemutatására: az adatvizualizáció elsősorban a tudósok körében, tudományos kiadványokban és rendezvényekben ismert volt. A 20. század első felében egy osztrák szociológus, Ottó Neurath létre hozta az úgynevezett ISOTYPE ikonografikai rendszert. Többek között például az oszlopdiagramokat úgy alakította át, hogy a hagyományos geometriai alakzatokat piktorikus ábrázolásokkal helyettesítette, ezt izotípus diagramként is ismerik. Ilyen módon a diagramokat egyben olyan szemléletes ábrákká is változtatta, amelyeket a nagyközönség könnyebben megért és elfogad. Ezeket a diagramokat napilapokban és magazinokban jelentették meg, ami egy fontos lépés volt a tudományok népszerűsítésében.
                  </p>`,
      bertin: `<h3>Bertin és a grafikai elmélet</h3>
                <p>A 20. századi adatvizuálizációról nem beszélhetünk a franciai Jacques Bertin (1915-2000) megemlítése nélkül. 1967-ben megjelent műve <i>(Sémiologie Graphique)</i> rendszerezte a grafikai megjelenítés elméletét. Többek között azt mondta ki, hogy a grafikának hét vizuális (szemmel érzékelhető) változója van, vagyis hét tulajdonságának a megváltoztatásával tudjuk ábrázolni az adatokat. Ezek a tulajdonságok pedig a következők: szín, mintázat, érték, méret, elhelyezés, alak és irány.
                </p>`,
    },
    first_thematic: {
      title: 'Az első tematikus térképek',
      first_thematic: `<h3>Az első tematikus térkép</h3>
                      <p>Az adatok grafikai megjelenítésének egyik hatékony eszköze a térkép. Ha térbelileg vagy földrajzi elterjedésükben akarjuk ábrázolni az adatokat, akkor elkerülhetetlen a térkép használata. Ezt a tudósok már századokkal ezelőtt felismerték: 1701-ben Edmund Halley (Anglia, 1656-1742) készítette az első térképet, amely a megszokott földrajzi adatokon kívül egy másik tudomány adatait is ábrázolta. Térképén a földi mágneses deklináció változását jelenítette meg, és ma ezt a művet tekintjük az első tematikus (szak)térképnek.</p>`,
      seaman: `<h3>Gyakorlati alkalmazás</h3>
                <p>Még a 18. században bebizonyosodott, hogy a tematikus kartográfiának egyszerre tudományos és gyakorlati alkalmazása is lehet. 1788-ban New Yorkban sárgaláz járvány tört ki, amely számos áldozatot szedett rövid idő alatt. Valentine Seaman (1770-1817) térképen ábrázolta a sárgaláz előfordulási helyeit, hogy ilyen módon ezeket könnyebben lehessen elkülöníteni és megakadályozzák a betegség elterjedését. Térképei voltak az első egészségügyi tematikájú térképek.</p>`,
      geological: `<h3>Korai földtani térképezés</h3>
                  <p>A tematikus térképek alkalmazása különböző tudományágakban töretlenül folytatódott és fejlődött a 18. és 19. században. Ennek egyik, nemzetközileg is elismert legjelentősebb példája az 1801-ben készített, de csak 1815-ben megjelent Anglia és Wales földtani térképe. Szerzője William Smith (Anglia, 1769-1839), aki elsőként alkalmazta a rétegek vizsgálatán alapuló földtani elemzést és ábrázolta ezt tematikus térképen. Művét az 1881-ben megszületett földtani jelkulcs elődjének tekintik.</p>`,
      berghaus: `<h3>Humboldt és Berghaus</h3>
                  <p>Az előző fejezetben megemlítettük Alexander von Humboldt nevét (1769-1859), mint a 19. században élt legnagyobb német földtudóst. Személye a tematikus térképészethez is kapcsolódik, mivel 1817-ben ő készítette az első világtérképet, ami a hőmérséklet változását ábrázolta. Ez a térkép az első természetföldrajzi atlaszban jelent meg, amelyet a német származású Heinrich Berghaus adott ki 1843-ban.</p>`,
      french: `<h3>A francia hatás</h3>
                <p>Franciaország is fontos szerepet vállalt a tematikus kartográfia 19. századbeli fejlődésében. 1819-ben Pierre Charles Dupin báró (1784-1873) elkészíti az első fekete-fehér kartogram-térképet, amelyen felületekre vonatkozó adatokat ábrázolt. Csak 11 évvel később Frère de Montizon rajzolja meg az első pontszórásos térképet, amelyen Franciaország járáskénti lakosságát úgy ábrázolta, hogy 1 pont 10 000 lakosnak felelt meg.</p>`,
      snow: `<h3>John Snow</h3>
              <p>1855-ben az angol John Snow (1813-1858) követi Valentine Seaman példáját és elkészíti az első járványügyi térképet Angliában. Ebben az időszakban kolera járvány tört ki Londonban, és Snow pontokkal jelölte ki a betegség gócpontjait a térképen, vagyis a város mely pontjain bukkant fel a betegség. Ez segítséget jelentett az egészségügyi hatóságoknak abban, hogy megakadályozzák a vírus elterjedését. Ezen kívül olyan térképet is készített, amelyen igyekezett bemutatni a kapcsolatot a betegség és a szennyezett vízzel ellátott területek között. Erről 
              <a href="https://www.eletestudomany.hu/snow_doktor_jarvanyos_katrografiaja" target="_blank" aria-label="external link on new tab"> bővebben itt <i class="fas fa-external-link-alt fa-sm"></i></a>
              olvashatsz.</p>`,
    },
    modern_thematic: {
      title: 'A mai tematikus térképészet',
      sciences: `<h3>Tudományok a térképeken</h3>
                <p>Az iskolai atlaszokból ismerünk olyan térképeket, amelyek természeti, gazdasági és társadalmi jelenségeket ábrázolnak tudományos adatok alapján. Ilyenek a középhőmérsékletet, az éghajlatot, a növényzetet, a mezőgazdaságot vagy a történelmi eseményeket bemutató térképek. Ha alaposan megfigyeljük őket, észrevehetjük, hogy nem csak tartalmukban, hanem ábrázolásukban is különböznek egymástól.</p>`,
      symbols: `<h3>Ábrázolás jelekkel</h3>
                <p>A térképeken nagyon gyakran használjuk a jeleket a tematikák ábrázolására. A képszerű jelek közül találunk szemléletes jeleket (amelyek a valódi objektumokhoz nagyon hasonlítanak) illetve egyszerűsített, vázlatszerű módon készített absztrakt jeleket. A mértani jelek is nagyon gyakoriak: ezek geometriai alakzatok (kör, négyzet, háromszög) különböző változatai. Nem szabad megfeledkeznünk a vonalas jelekről, amelyek segítségével közigazgatási határokat (megye- vagy országhatárokat), úthálózatot stb. ábrázolhatunk.</p>`,
      polygons: `<h3>Felületek ábrázolása</h3>
                <p>Jeleket is alkalmazhatunk, amikor felületi kiterjedésű jelenségeket ábrázolunk, hogy ilyen módon jól megkülönböztethessük egymástól ezeket. De leggyakrabban színeket használunk a felületek elkülönítésére, például a különböző talajok, éghajlatok vagy nyelvek elterjedésének az ábrázolására.</p>`,
      isoline: `<h3>Izovonalak</h3>
                <p>Olyan jelenségek is előfordulnak egy bolygó felszínén, amelyek folytonosan változnak. Ezt a változást több helyen végzett mérésekkel lehet meghatározni. Az azonos értékű pontokat vonalakkal kötik össze, ezeket izovonalaknak hívják. Legismertebb alkalmazása a domborzat ábrázolása, de több természeti folyamatot is szokás ábrázolni ezzel a módszerrel (például a hőmérséklet vagy a csapadék változását).</p>`,
      diagrams: `<h3>Diagramok a térképeken</h3>
                <p>Ha egy pontra vagy felületre vonatkozó adatokat részletesen akarunk ábrázolni, akkor ezt diagramok segítségével megtehetjük. A tematikus térképeken a két leggyakoribb diagramtípus az oszlop- és kördiagram, amelyeknek több alfajtája van. A jól szerkesztett diagramok jó áttekintést adhatnak az ábrázolt adatok területi elterjedéséről, de lényeges a pontos adatok visszaadása is. Emiatt (ha a térkép méretaránya megengedi és nem zavarja a térkép olvashatóságát) a diagramokon is feltüntethetjük ezeket az értékeket.</p>`,
      cartograms: `<h3>Kartogramok</h3>
                  <p>Akkor használjuk ezt a módszert, ha az adatainkat nem lehet pontosan vonatkoztatni egy pontra a térképen, azaz felületre vonatkoznak. Egy speciális változata az úgynevezett torzított felületi kartogram, amikor az eredeti földrajzi felület alakjának a megváltoztatásával (vagy más geometriai, képszerű alakzattal való helyettesítéssel) fejezzük ki az értékbeli különbségeket.</p>`,
      points: `<h3>Ábrázolás pontokkal</h3>
              <p>A pontokat értékegységként is használhatjuk a térképeken annak érdekében, hogy a kiválasztott tematika földrajzi eloszlását szemléletesen bemutassuk. Minden egyes, ugyanolyan méretű ponthoz rendelünk egy értéket (például egy pont megfelelhet 1000 lakosnak), és a pontokat szétszórva, a vonatkozási felületen véletlenszerűen helyezzük el. E módszernek két változata is lehet: amikor más-más színekkel vagy más alakzatokkal (háromszög, négyzet) ugyanazon térképen ábrázolunk különböző tematikákat, vagy különböző nagyságú és értékű pontokat alkalmazunk.</p>`,
      movement: `<h3>A mozgás szemléltetése</h3>
                <p>Ha egy történelmi térképen a hadsereg helyváltoztatását akarjuk ábrázolni, vagy egy földrajzi térképen a tengeráramlások irányát akarjuk megadni, akkor ezt nyilak segítségével tehetjük meg. Ez az úgynevezett mozgásvonalak módszere, amellyel a térbeli elmozdulást ábrázoljuk. A nyílfejjel az irányt, a szélességgel a mennyiséget, a színével pedig valamilyen tulajdonságot adhatunk meg.</p>`,
    },
    traditional_multi: {
      title: 'Hagyományos multimédia a térképeken',
      before: `<h3>A mai multimédia előtt…</h3>
                <p>A multimédia szó csak az utolsó 20-30 évben lett szélesebb körben ismert, ami elsősorban a személyi számítógépek tömeges elterjedéséhez kapcsolódik. Emiatt sokan a multimédiát kizárólag a digitális módon tárolt adatok grafikai (többoldalú) megjelenítéséhez kötik, de az emberiség már hosszú évszázadok óta többféleképpen igyekszik átadni és megörökíteni a megszerzett információkat. Az adatokat először elmeséléssel, írással és grafikával közvetítették, később animációt és mozgóképeket is használtak erre a célra. A honlap 1. fejezetében néhány példán keresztül összefoglalva mutattuk meg, hogyan történt a tudományos adatok grafikai ismertetése a 14. századtól napjainkig.</p>`,
      multimedia: `<h3>Mi a multimédia?</h3>
                  <p>A multi szótag „sokat” jelent, még a médiát eszközként is értelmezhetjük. Ilyen módon multimédia alatt azt értjük, hogy az információt többféleképpen vagy több eszköz (szöveg, grafika, hang, animáció és videó) segítségével közvetíthetjük. Ha ezt számítógép nélkül valósítjuk meg, akkor hagyományos multimédiáról beszélhetünk. Ez a fajta multimédia volt az uralkodó egészen az 1980-as évekig.</p>`,
      maps: `<h3>Térképek a multimédiában</h3>
              <p>A hagyományos multimédia mindenekelőtt a grafikán alapult, amely a szöveges magyarázatokat érthetőbbé teszi. A grafika egy nagyon széleskörű meghatározás, amely többek között tartalmazza a képeket, fényképeket, diagramokat, metszeteket és természetesen a térképeket is. A 3. fejezetben vázlatosan ismertetett tematikus kartográfia több grafikai kifejezési eszközt (ábrázolási módszert) foglal magában, amely a piktorikus (képszerű) jelektől a bonyolultabb diagramokig terjed. Ezek mind olyan részét képezik a grafikai multimédiának, amely az adatok térbeli ábrázolását teszi lehetővé.</p>`,
      atlases: `<h3>Atlaszok</h3>
                <p>Egy nyomtatott atlasz tekinthető a multimédia látványosan oktató eredményének: benne harmonikusan „keverednek” a térképek, diagramok, fényképek, rajzok, metszetek stb. Mindez annak érdekében történik, hogy lehetőleg a legdidaktikusabb eszközökkel, a nagyközönség részére is érdekébresztő módon mutathassuk be a földünk felszínét, élővilágát, országait.</p>`,
      animation: `<h3>Az első animációs térképek</h3>
                  <p>A mozgókép megjelenése a multimédiában a 20. század jelensége, amely a térképészet területén a múlt század közepén jelenik meg. A második világháború alatt az amerikai kormány háborús propagandafilmek készítésével bízta meg a Walt Disney Stúdiót és Frank Capra amerikai rendezőt, hogy az önkéntes katonák toborzása alatt vetítésre kerüljenek. Az alkotók felismerték és kihasználták a térkép és az animáció együttes alkalmazásában rejlő lehetőségeket: ezekben a 40-es évek első felében készített filmekben láthattunk először olyan egyszerű animációs térképeket, amelyek megmutatták a harcokban levő csapatok mozgását, a légi támadások menetét stb. 
                  <a href="http://www.archive.org/movies/thumbnails.php?identifier=DivideAndConquer" target="_blank" aria-label="external link on new tab">Itt tudod megnézni <i class="fas fa-external-link-alt fa-sm"></i></a>
                   a Capra által rendezett <i>"Why We Fight"</i> propaganda sorozat harmadik részét.</p>`,
    },
    digital_multi: {
      title: 'Digitális multimédia a térképeken',
      introduction: `<h3>Bevezetés</h3>
                    <p>A nyomtatott atlasz egy multimédiás termék, de nem képes kihasználni a modern multimédia által kínált összes lehetőséget, például az animációt. Ha egy hagyományos atlaszban olyan folyamatot ábrázolunk, amely több éven vagy évszázadon keresztül megy végbe (például egy város fejlődését), akkor ezt rendszerint egy teljes oldalon, több térkép segítségével lehet bemutatni. Ha digitális multimédiát használunk, ugyanezt egy egyszerű animációval meg lehet oldani.</p>`,
      beginnings: `<h3>A digitális multimédia kezdetei</h3>
                    <p>A digitális térképek készítése a 20. század 60-as éveiben kezdődött, az első térinformatikai rendszer megjelenésével Kanadában <i>(Canada Land Inventory)</i>. Erről készült egy kisfilm is, ez három részben van fent a Youtube-on (
                    <a href="https://www.youtube.com/watch?v=eAFG6aQTwPk" target="_blank" aria-label="external link on new tab">1.rész <i class="fas fa-external-link-alt fa-sm"></i></a>, 
                    <a href="https://www.youtube.com/watch?v=3kFYsOHgDSo" target="_blank" aria-label="external link on new tab">2.rész <i class="fas fa-external-link-alt fa-sm"></i></a>, 
                    <a href="https://www.youtube.com/watch?v=ryWcq7Dv4jE" target="_blank" aria-label="external link on new tab">3.rész <i class="fas fa-external-link-alt fa-sm"></i></a>).
                    <br>
                    Az első számítógépes animáció készítése szintén a 60-as évek elején valósult meg: 1962-ben az USA-beli AT&Bell laboratóriumban elkészült az első animált diagram, amely egy vonal- és pontdiagram kombinációjával megmutatta az adatok időbeli változását. Ez a körülbelül 1 perces videó volt az első lépés az adatok dinamikus ábrázolásának a fejlődésében.
                    <br>
                    Ebben a videóban egy másik korai animációt láthattok.</p>`,
      maps: `<h3>A digitális térképek</h3>
              <p>Digitális térképeket négyfajta szoftverrel készítünk: általános grafikai (CorelDraw, Adobe Illustrator), térinformatikai (ArcGIS, QGIS, Geomedia, Global Mapper, Mapinfo), térképészeti (OCAD) és mérnöktervezői (AutoCAD). Ezekben a programokban találhatunk olyan multimédiás lehetőségeket, amelyek színesítik és gazdagítják az adatok ábrázolását: mindegyikkel hozzáadhatunk képeket, hangokat és videókat a térképeinkhez, és emellett diagramokat, 3D modelleket és animációkat is készíthetünk.</p>`,
      atlases: `<h3>Multimédiás digitális atlaszok</h3>
                <p>A digitális kartográfia fejlődése a multimédiás digitális atlaszok megjelenéséhez is vezetett. Ezek az atlaszok a nyomtatott hagyományos atlaszok továbbfejlesztései. A digitális feldolgozás olyan előnyöket kínál, amiket nem találhatunk a nyomtatott atlaszokban: a térképeket nagyobb számú és jobb minőségű képekkel, valamint videórészletekkel egészíthetjük ki. A digitális atlasz kezelése interaktívvá vált: többek között a felhasználó kiválaszthatja, melyik térképet akarja kinyomtatni, vagy milyen tartalommal akarja ellátni a térképet, mivel a digitális atlaszok többsége megengedi a kiválasztott információk (domborzat, vízrajz, határok stb.) ki-bekapcsolását. Egy példa multimédiás digitális atlaszra  
                <a href="https://atlasderschweiz.ch/" target="_blank" aria-label="external link on new tab">Svájc atlasza <i class="fas fa-external-link-alt fa-sm"></i></a>.</p>`,
    },
    webcartography: {
      title: 'Multimédia a webkartográfiában',
      maps:`<h3>Térképek a weben</h3>
            <p>Ma már a világhálón keresztül is egyre több térképészeti termék elérhető digitális atlasz, didaktikai játék vagy ismeretterjesztés formájában. A kutatómunkahelyek szintén a Webre teszik fel a fejlesztett alkalmazásaikat és kutatási eredményeiket. Ebben a fejezetben néhány példát mutatunk a webkartográfia számtalan felhasználhatóságára.</p>`,
      interactivity: `<h3>Interaktivitás a webatlaszokban</h3>
                      <p>A weben keresztül elérhető térképszolgáltatások lehetővé teszik a térképek egyszerű szerkesztését is. Ezt nevezzük interaktivitásnak: maga a felhasználó eldöntheti, hogy a felkínált tematikák közül melyiket akarja megjeleníteni és később kinyomtatni, ha úgy kívánja. Ehhez hozzá kell tennünk, hogy a felhasználó kiválaszthatja az alaptérkép elemeit: legyen-e rajta domborzat, vízrajz, úthálózat stb. Erre egy példa a 
                      <a href="https://apps.nationalmap.gov/viewer/" target="_blank" aria-label="external link on new tab">USGS National Map Viewer <i class="fas fa-external-link-alt fa-sm"></i></a>.</p>`,
      arcgis_online: `<h3>ArcGIS Online</h3>
                      <p>Az ArcGIS Online-t az ESRI amerikai cég fejleszti. Az alkalmazásaival különféle webes térképeket, térképes applikációkat lehet készíteni, amiket utána beágyazhatunk például a weboldalunkba. 
                      <br>
                      A Nearby applikációval olyan térképet készíthetünk, aminek a segítségével a felhasználó meg tudja keresni, hogy egy adott ponttól bizonyos távolságra milyen üzletek, parkok, uszodák stb. vannak.
                      <br>
                      A StoryMaps lényege, hogy olyan applikációkat készítsünk, amivel valamilyen történetet mondunk el térképekkel és multimédiás tartalmakkal kiegészítve. 
                      <br>
                      A Dashboard applikációval különféle diagramokat, elemzéseket jeleníthetünk meg. 
                      <br>
                      Ezeken kívül még sok más alkalmazás van, mindegyikkel más célnak megfelelő térképes applikációkat lehet készíteni.</p>`,
      real_time: `<h3>Valós idejű adatok a térképeken</h3>
                  <p>A webes térképészet elterjedésével ma már olyan térképek is léteznek, amik közel valós idejű adatokat jelenítenek meg. 
                  <br>
                  Ezek célja gyakran, hogy valamilyen járművek mozgását tudjuk nyomon követni, mint a 
                  <a href="https://www.vesselfinder.com/" target="_blank" aria-label="external link on new tab"> Vesselfinder <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , amivel hajók, vagy a 
                  <a href="https://www.flightradar24.com" target="_blank" aria-label="external link on new tab"> Flightradar <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , amivel repülők mozgását tudjuk valós időben figyelni.
                  <br>
                  De ilyenek a 
                  <a href="https://earth.nullschool.net/" target="_blank" aria-label="external link on new tab"> Föld időjárási és óceáni tényezőit megjelenítő oldal <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , ami az adatokat animáció formájában mutatja be, illetve a 
                  <a href="https://github.com/" target="_blank" aria-label="external link on new tab"> Github főoldalán <i class="fas fa-external-link-alt fa-sm"></i></a>
                   látható földgömb is. </p>`,
      senseable: `<h3>Senseable City</h3>
                  <p>A Senseable City Laboratory a Massachusetts Institute of Technology kutatási kezdeményezése. A labor célja annak vizsgálata, hogy a digitális technológiák hogyan változtatják meg az emberek életmódját és előrejelezze ennek következményeit.
                  <br>
                  <br>
                  Néhány a projektjeik közül: egy 
                  <a href="https://senseable.mit.edu/desirable-streets/" target="_blank" aria-label="external link on new tab">térkép <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , amely bemutatja, hogy melyik utcákat részesítik előnyben az emberek, és összehasonlítja egy adott úticélhoz vezető leginkább előnyben részesített utat a legrövidebb úttal, egy animált 
                  <a href="https://senseable.mit.edu/roboat_summer_day/" target="_blank" aria-label="external link on new tab">térkép <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , amin a csónakok és hajók mozgását láthatjuk Amszterdam csatornáin és egy olyan 
                  <a href="https://senseable.mit.edu/mit-world" target="_blank" aria-label="external link on new tab">térkép <i class="fas fa-external-link-alt fa-sm"></i></a>
                 , amely megmutatja az MIT-n tanuló nemzetközi hallgatók számát és, hogy honnan érkeztek.</p>`,
      vakeger: `<h3>Vakegér: vaktérképes játék</h3>
                <p>Az ELTE 
                <a href="http://lazarus.elte.hu/" target="_blank" aria-label="external link on new tab">Térképtudományi és Geoinformatikai Intézet régi honlapján <i class="fas fa-external-link-alt fa-sm"></i></a>
                 elérhető néhány interaktív alkalmazás. Az egyik a tanszék tanárai és doktoranduszai által fejlesztett 
                <a href="http://lazarus.elte.hu/vakeger/" target="_blank" aria-label="external link on new tab">Vakegér vaktérképes játék <i class="fas fa-external-link-alt fa-sm"></i></a>. 
                Három, egymástól független játék gyűjteménye: a vaktérképes játék, a vakegér 3D és a vakegér a Marson. A vaktérképes játék használata nagyon egyszerű: a kezdőoldalon kell bejelentkezni játékosként, utána megadni milyen szinten akarunk játszani, illetve a kiválasztott szinttől függően milyen elemeket tartalmazzon a vaktérkép és milyen kategóriákban (például települések és/vagy világörökségek) kérdezzen meg bennünket. Ezután megjelenik a névrajz nélküli térkép, és kezdhetjük ráhelyezni az oldalt felsorolt neveket.</p>`,
    },
    mapping_services: {
      title: 'Webes térképszolgáltatások',
      mapping: `<h3>Webes térképszolgáltatások</h3>
                <p>Webes térképszolgáltatások alatt azokat a földrajzi információs rendszer (GIS) alapú fejlesztéseket értjük, amik az interneten keresztül szolgáltatnak térképeket. Megjelenésükkel számos földrajzi adatkészletet is létrejött, köztük az OpenStreetMap ingyenes adatai és a HERE, Google, TomTom és más cégek tulajdonában lévő adatok.<p>`,
      google: `<h3>Google Maps</h3>
               <p> A Google Maps a legelterjedtebb webes térképszolgáltatás. Műholdas képeket, légi fényképeket, utcatérképeket, 360°-os interaktív panorámás utcaképeket (Street View), valós idejű forgalmi viszonyokat és útvonaltervezést is kínál. Emellett képeket és videókat is fel lehet tölteni, illetve saját storymap-eket készíteni a Google My Maps nevű alkalmazással.</p>`,
      osm: `<h3>OpenStreetMap</h3>
            <p>Az OpenStreetMap (OSM) is egy jól ismert térképszolgáltatás. A célja egy ingyenes, szerkeszthető világtérkép létrehozása. A térkép alapjául szolgáló geoadatokat tekintik a projekt legfőbb eredményének. A felhasználók is vehetnek fel adatokat, amiket az Open Database License alatt tesznek elérhetővé.</p>`,
      other: `<h3>Egyéb térképszolgáltatások</h3>
              <p>Vannak más térképszolgáltatások is, mint például az Apple Maps, a Bing Maps és a Here WeGo. 
              <br>
              Az Apple Maps az Apple cég által fejlesztett operációs rendszerek alapértelmezett térképrendszere.
              <br>
              A Bing Maps a Microsoft Bing keresőmotorjának része. 
              <br>
              A HERE WeGo egy webes térképészeti és navigációs szolgáltatás, amelyet a Here Technologies üzemeltet. Ennek az adatait is a HERE biztosítja, ezek többek között műholdas felvételeket, forgalmi adatokat és más helymeghatározás alapú szolgáltatásokat tartalmaznak.</p>`,
    },
    modern_technologies: {
      title: 'Modern technológiák',
      AR: `<h3>Mi a kiterjesztett valóság?</h3>
          <p>Kiterjesztett valóságnak, (angolul <i>Augmented Reality</i>, AR), hívjuk a valós fizikai környezetnek azt a képét, amit virtuális elemekkel egészítük ki. Ezek a virtuális elemek lehetnek például képek, modellek, videók vagy akár hangok is.
          <br>
          Az AR hasonló a virtuális valósághoz (VR), de míg az utóbbi célja, hogy minél inkább csak a virtuális világot érzékeljük, addig az AR-ben a valós világot kiegészítjük virtuális elemekkel és mind a kettőt egyszerre érzékeljük. 
          <br>
          Általában okostelefonra vagy tabletre telepített alkalmazás segítségével lehet a kiegészített valóságot megtapasztalni, de készítenek okos szemüvegeket és szem elé vetített kijelzőket (HUD) is.</p>`,
      marker: `<h3>Képfelismerés alapú AR</h3>
              <p>Ehhez a fajta kiterjesztett valósághoz valamilyen speciális vizuális jelölőre van szükség, ez lehet QR kód, de lehet térkép is vagy bármilyen más grafika. Az applikáció a készüléken lévő kamerával felismeri a jelölőt és megjeleníti a hozzákapcsolódó információkat. Meg tudja állapítani a jelölő helyzetét és tájolását, és e-szerint helyezi el a tartalmat is.
              <br>
              <br>
              Az <a href="https://elsa3dmap.com/" target="_blank" aria-label="external link on new tab">Elsa 3D Map <i class="fas fa-external-link-alt fa-sm"></i></a>
               cég is ilyen fajta kiterjesztett valóságot használ, hogy a térképeken megjelenítse a tematikát vagy hogy térképes kirakót készítsen.
              <br>
              <br>
              Másik példa a 
              <a href="https://the7thcontinent.seriouspoulp.com/en/resources/mobile_app" target="_blank" aria-label="external link on new tab">7. kontinens társasjáték mobilos applikációja <i class="fas fa-external-link-alt fa-sm"></i></a>. Ha a mobilt a térkép megfelelő része fölé helyezzük, akkor valamilyen virtuális elem jelenik meg az adott helyen.</p>`,
      location: `<h3>Helymeghatározás alapú AR</h3>
                <p>A helymeghatározás alapú kiterjesztett valóságok a felhasználó eszközének GPS-ét, iránytűjét, giroszkópját és gyorsulásmérőjét használják arra, hogy megállapítsák az eszköz  helyzetét és attól függően különböző virtuális tartalmakat jelenítsenek meg. Leggyakrabban a közelben lévő üzletek vagy tervezett útvonal megjelenítésére használják.
                <br>
                <br>
                Sok AR-alkalmazás van a csillagképek megtekintésére és azonosítására is. A 
                <a href="https://play.google.com/store/apps/details?id=com.vitotechnology.StarWalk2Free&hl=en&gl=US" target="_blank" aria-label="external link on new tab"> Star Walk 2 <i class="fas fa-external-link-alt fa-sm"></i></a>
                 is olyan alkalmazás, aminek AR funkciója is van a sok más egyéb mellett. Használatakor a valódi égbolt képére, egy csillagtérképhez hasonlóan, vetítjük rá például a csillagképek nevét, képét.</p>`,
      sandbox: `<h3>Augmented Reality Sandbox</h3>
                <p>Az <a href="https://arsandbox.ucdavis.edu/" target="_blank" aria-label="external link on new tab">AR Sandbox <i class="fas fa-external-link-alt fa-sm"></i></a>
                 (homokasztal) segítségével homok formázásával topográfiai modelleket lehet készíteni.
                <br>
                Vetítőt és mozgásérzékelőt (Kinect 3D kamera) használ, amik egy "homokozó" fölé vannak szerelve. A kamera érzékeli az alatta lévő homoktól való távolságot és a projektor egy szintvonalakkal és színtérképpel ellátott magassági modelt vetít a homok felületére. Amikor a kamera valamilyen tárgyat (például egy kezet) érzékel egy adott magasságban a homok felett, akkor az alatta lévő felületen virtuális eső jelenik meg.</p>`,
    },
    contact: {
      title: 'Kapcsolat',
      address: `Cím: 1117 Budapest, Pázmány Péter sétány 1/A<br>
                Postacím: 1518 Budapest, Pf. 32.<br>
                Közvetlen telefonszám: 3722975<br>
                Fax: 3722951<br>
                Email: terktud{'@'}ludens.elte.hu<br>`,
    },
    attributions: {
      title: 'Források',
      content: `<p>
                  <b>Modern technológiák téma forrása:</b> 
                  <br>
                  Yonov, Nikola. (2019). School Atlas with Augmented Reality.
                  <br>
                  Proceedings of the ICA. 2. 1-6. 10.5194/ica-proc-2-150-2019.
                </p>
                <p>
                  <b>Képek forrása főként:</b>
                  <br>
                  Wikimedia Commons, David Rumsey Térképgyűjtemény, középiskolai atlaszok
                  <br>
                  Más képeket, videókat én készítettem az adott szoftverről.
                </p>                
                <p>
                  A háttérképeket az 
                  <a href="https://alvarcarto.com/phone-background/" target="_blank" aria-label="external link on new tab">Alvar Carto <i class="fas fa-external-link-alt fa-sm"></i></a>
                  ingyenes háttérképkészítőjével csináltam.
                </p>                
                <p>
                  kartogramok 1.kép: készítette Toby Hudson
                  <br>
                  <a href="https://creativecommons.org/licenses/by-sa/3.0/au/" target="_blank" aria-label="external link on new tab">CC BY-SA 3.0 AU <i class="fas fa-external-link-alt fa-sm"></i></a>
                </p>
                <p>
                  kartogramok 2.kép: készítette Derek Montaño
                  <br>
                  kartogramok 4.kép: készítette ThePromenader
                  <br>
                  kartogramok 5.kép: készítette Moyogo
                  <br>
                  <a href="https://creativecommons.org/licenses/by-sa/3.0/" target="_blank" aria-label="external link on new tab">CC BY-SA 3.0 <i class="fas fa-external-link-alt fa-sm"></i></a>
                </p>
                <p>
                  ábrázolás pontokkal 1.kép: készítette Shayna Fever
                  <br>
                  ábrázolás pontokkal 2.kép: készítette Kelly Ziegenfuss
                  <br>    
                  <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" aria-label="external link on new tab">CC BY-SA 4.0 <i class="fas fa-external-link-alt fa-sm"></i></a>
                </p>`,
    },
  },
  
  en: {
    footer: 'ELTE, Institute of Cartography and Geoinformatics',
    header: {
      title: 'Multimedia on maps',
      tooltip_theme: 'Theme toggle',
    },
    home: {
      title: 'Homepage',
      introduction: `<h3 style="text-align: left">Welcome to the website!</h3>
                    This website is part of a "series of websites" on cartography and geoinformatics. In this section you can read about what kind multimedia content can be displayed on maps, while the two other parts of the series are about the
                    <a href="http://lazarus.elte.hu/hun/dolgozo/jesus/tt/kezdes.htm" target="_blank" aria-label="external link on new tab">history of cartography <i class="fas fa-external-link-alt fa-sm"></i></a> 
                    and 
                    <a href="http://lazarus.elte.hu/hun/dolgozo/jesus/terinfo/kezdes.htm" target="_blank" aria-label="external link on new tab">geoinformatics <i class="fas fa-external-link-alt fa-sm"></i></a>. The original websites were created around 2006, and two of them, including this site, were modernized in 2021 as part of a dissertation.
                    <br>
                    <br>
                    The website was designed primarily for high school students, trying to summarize the topics in a concise, understandable and interesting way. The content is divided into 8 major chapters, within which are further minor topics. These include among others data visualization, web cartography, and augmented reality.`
    },
    navbar: {
      home: 'Home',
      content: 'Content',
      contact: 'Contact'
    },
    sidenav: {
      data: 'The visualization of data',
      first_thematic_maps: 'First thematic maps',
      modern_thematic_maps: 'Modern thematic maps',
      traditional_multimedia: 'Traditional multimedia on maps',
      digital_multimedia: 'Digital multimedia on maps',
      web_cartography: 'Multimedia in web cartography',
      mapping_services: 'Web mapping services',
      modern_technologies: 'Modern technologies',
    },
    data: {
      title: 'The visualization of data',
      catalan: `<h3>Data Visualization</h3>
                <p>From an early age, scientists have sought to record their observations not only with data, formulas, or descriptions, but also to record the results of their research graphically, this is now called data visualization. One of the best example of this is the <i>Catalan Atlas</i> made in 1375 by Abraham Cresques. In this we can find diagrams made independently of the maps, which illustrate, among others, contemporary ideas about the Universe or the tidal phenomenon.
                </p>`,
      graunt: `<h3>Early datasets</h3>
                <p>The earliest predecessor of today's statistical yearbooks and digital databases was born in 1662. In that year, John Graunt (England, 1620-1674) recorded the mortality rate of the population of London. He organized this data into topics and tables and published it in a single volume. These data are still valuable to researchers: their analysis revealed, among other things, that in the second half of the 17th century, the average age in London was 27 years, and 65% of the city’s population died before the age of 16.
                </p>`,
      playfair: `<h3>New methods of graphic display</h3>
                <p>After John Graunt, the collection of statistical data extended to other economic areas. The need to make these data and the correlations between them easier to understand has slowly arisen. To do this, graphic tools were used: in 1786, a Scottish engineer, William Playfair (1759-1823), was the first to use bar, pie, and line charts to present economic data.
                </p>`,
      humboldt: `<h3>Alexander von Humboldt</h3>
                  <p>The “diagrammatic” display of data has also become increasingly popular in the natural sciences. The German Alexander von Humboldt, who had significant achievements in several branches of the natural sciences, undertook a number of these tasks in the first half of the 19th century. He was the first who plotted the change in temperature as a function of latitude and longitude using a diagram, but he also used graphical representation in other disciplines (for example in Mexico to compare population and area size).
                  </p>`,
      pictorial: `<h3>Promoting charts</h3>
                  <p>In the 19th century, the use of diagrams to present scientific results became commonplace: data visualization was known primarily among scientists, in scientific publications and events. 
                  In the first half of the 20th century, an Austrian sociologist, Otto Neurath, created the so-called ISOTYPE iconographic system. Among other things, for example, he transformed bar graphs by replacing traditional geometric shapes with pictorial representations, these are known as isotype diagrams. This way, he made the diagrams more illustrative that are easier for the general public to understand and accept. These charts were published in newspapers and magazines, which was an important step in popularizing the sciences.
                  </p>`,
      bertin: `<h3>Bertin and the graphic theory</h3>
                <p>We cannot talk about data visualization in the 20th century without mentioning the French Jacques Bertin (1915-2000). His work <i>(Sémiologie Graphique)</i>, published in 1967, systematized the theory of graphic representation. Among other things, he said that graphics have seven visual (perceptible) variables, meaning that we can represent data by changing seven properties. These properties are color, pattern, value, size, placement, shape, and direction.
                </p>`,
    },
    first_thematic: {
      title: 'The first thematic maps',
      first_thematic: `<h3>The first thematic map</h3>
                      <p>One of the effective tools for graphical display of data is the map. If we want to represent the data spatially or geographically, it is inevitable to use a map. Scientists recognized this centuries ago: in 1701, Edmund Halley (England, 1656-1742) made the first map, which depicted data from another science in addition to the usual geographical data. He showed the change in the earth's magnetic declination on his map, and today we consider this work to be the first thematic map.</p>`,
      seaman: `<h3>Practical application</h3>
                <p>In the 18th century it was proved that thematic cartography can have both scientific and practical applications. In 1788, a yellow fever epidemic broke out in New York, which took many victims in a short time. Valentine Seaman (1770-1817) depicted the sites of yellow fever on a map so that they could be more easily isolated and thus prevent the spread of the disease. His maps were the first health-themed maps.</p>`,
      geological: `<h3>Early geological mapping</h3>
                  <p>The use of thematic maps in various disciplines continued unabated and developed in the 18th and 19th centuries. One of the most internationally recognized examples of this is the geological map of England and Wales, which was made in 1801 but published only in 1815. Its author was William Smith (England, 1769-1839), who was the first to apply and depict geological analysis based on the study of strata on a thematic map. His work is considered the predecessor of the geological map key adopted in 1881.</p>`,
      berghaus: `<h3>Humboldt and Berghaus</h3>
                  <p>In the previous chapter we mentioned the name of Alexander von Humboldt (1769-1859) as the greatest German earth scientist who lived in the 19th century. His person is also associated with thematic cartography, since in 1817 he made the first world map depicting changes in temperature. This map appeared in the first natural geographic atlas published in 1843 by the German Heinrich Berghaus.</p>`,
      french: `<h3>The French influence</h3>
                <p>France has also played an important role in the development of thematic cartography in the 19th century. In 1819, Baron Pierre Charles Dupin (1784-1873) made the first black-and-white choropleth map on which he depicted data related to surfaces. It was only 11 years later that Frère de Montizon drew the first scatter map, on which he depicted the population per district in France as 1 point per 10,000 inhabitants.</p>`,
      snow: `<h3>John Snow</h3>
              <p>In 1855, the English John Snow (1813-1858) followed the example of Valentine Seaman and made the first epidemiological map of England. During this period, a cholera epidemic broke out in London, and Snow marked the focal points of the disease with points on the map, that is, at which points in the city the disease appeared. This has helped health authorities prevent the virus from spreading. He also produced a map that sought to show the connection between the disease and areas with contaminated water. You can 
              <a href="https://www.eletestudomany.hu/snow_doktor_jarvanyos_katrografiaja" target="_blank" aria-label="external link on new tab">read more about this topic here <i class="fas fa-external-link-alt fa-sm"></i></a>.</p>`,
    },
    modern_thematic: {
      title: 'Modern thematic maps',
      sciences: `<h3>Sciences on maps</h3>
                <p>From school atlases, we know maps that depict natural, economic, and social phenomena based on scientific data. These include maps showing average temperature, climate, vegetation, agriculture, or historical events. If we observe them closely, we can notice that they differ not only in their content but also in their representation.</p>`,
      symbols: `<h3>Depiction with symbols</h3>
                <p>We often use symbols on maps to represent thematics. Among the pictorial symbols we find illustrative symbols (which are very similar to the real objects) and abstract symbols made in a simplified, sketchy way. Geometric symbols are also very common: they are different versions of geometric shapes (circle, square, triangle). We must not forget that lines can be used to represent administrative boundaries (county or country borders), the road network, and so on.</p>`,
      polygons: `<h3>Representation of surfaces</h3>
                <p>Symbols can also be used to represent surface-wide phenomena so that they can be well distinguished from each other. But most often, colors are used to separate surfaces, such as to represent the spread of different soils, climates, or languages.</p>`,
      isoline: `<h3>Isolines</h3>
                <p>There are also phenomena on the surface of a planet that are constantly changing. This change can be determined by measurements at multiple locations. Points of equal value are connected with lines, called isolines. Its best-known application is to represent topography, but several natural processes are usually represented by this method (for example changes in temperature or precipitation).</p>`,
      diagrams: `<h3>Charts on maps</h3>
                <p>If you want to plot data for a point or surface in detail, you can do so with the help of diagrams. On thematic maps, the two most common chart types are bar charts and pie charts, which have several subtypes. Well-edited diagrams can give a good overview of the spatial distribution of the plotted data, but it is also important to return accurate data. For this reason (if the scale of the map allows and does not interfere with the readability of the map) we can also show these values on the charts themselves.</p>`,
      cartograms: `<h3>Choropleth maps</h3>
                  <p>We use this method if our data cannot be accurately referenced to a point on the map, because they relate to a surface. A special version of this is the so-called area cartogram, when the differences in value are expressed by changing the shape of the original geographical surface (or by replacing it with another geometric, pictorial shape).</p>`,
      points: `<h3>Representation with points</h3>
              <p>Points can also be used as a unit of value on maps in order to illustrate the geographical distribution of selected topics. Each point of the same size is assigned a value (for example, a point can correspond to 1000 inhabitants), and the points are scattered randomly on the reference surface. There can be two versions of this method: when we represent different thematics on the same map with different colors or shapes (triangles, squares), or we use points of different sizes and values.</p>`,
      movement: `<h3>Illustration of movement</h3>
                <p>If we want to show the relocation of the army on a historical map, or want to give the direction of sea currents on a geographical map, we can do this with the help of arrows. These are the so-called flow maps, which are used to plot spatial displacement. You can use the arrowhead to specify the direction, the width to specify the quantity, and the color to specify a property.</p>`,
    },
    traditional_multi: {
      title: 'Traditional multimedia on maps',
      before: `<h3>Before today's multimedia…</h3>
                <p>The word multimedia has only become more widely known in the last 20-30 years, primarily related to the spreading of personal computers. For this reason, many people associate multimedia solely with the graphical (multifaceted) display of digitally stored data, but humanity has been trying to convey and capture acquired information in many ways for many centuries. The data were first conveyed through storytelling, writing, and graphics, and later animation and motion pictures were also used for this purpose. In Chapter 1 of this website, we have summarized, through some examples, how scientific data has been graphically represented from the 14th century to the present day.</p>`,
      multimedia: `<h3>What is multimedia?</h3>
                  <p>"Multi" means “more” and "media" can be interpreted as a tool. So multimedia means that information can be conveyed in several ways or by means of several tools (text, graphics, sound, animation and video). If we do this without a computer, we can talk about traditional multimedia. This type of multimedia was dominant until the 1980s.</p>`,
      maps: `<h3>Maps in multimedia</h3>
              <p>Traditional multimedia was primarily based on graphics, which makes explanations explained with text more understandable. Graphics are a very broad definition that includes images, photographs, diagrams, sections, and of course maps, among other things. Thematic cartography, which we talked about in Chapter 3, includes several graphical means of expression (representation methods) ranging from pictorial symbols to more complex diagrams. They are all part of graphic multimedia that allows spatial representation of data.</p>`,
      atlases: `<h3>Atlases</h3>
                <p>A printed atlas can be considered the educational result of multimedia: maps, diagrams, photographs, drawings, engravings, etc. are harmoniously “mixed” in it. All of this is done in order to show, the surface, the wildlife or the countries of our planet in a way that also attracts the interest of the general public.</p>`,
      animation: `<h3>The first animated maps</h3>
                  <p>The emergence of motion pictures in multimedia is a phenomenon of the 20th century that appeared in the field of cartography in the middle of the last century. During World War II, the U.S. government commissioned Walt Disney Studio and U.S. director Frank Capra to make propaganda films to be screened during the recruitment of volunteer soldiers. The creators recognized and exploited the potential of using map and animation together: in these films made in the first half of the 1940s, we could see for the first time simple animated maps that showed the movement of troops in combat, the course of air attacks etc. 
                  <a href="http://www.archive.org/movies/thumbnails.php?identifier=DivideAndConquer" target="_blank" aria-label="external link on new tab">Here you can watch the third film <i class="fas fa-external-link-alt fa-sm"></i></a>
                  of the propaganda series <i>"Why We Fight"</i> which was directed by Capra.</p>`,
    },
    digital_multi: {
      title: 'Digital multimedia on maps',
      introduction: `<h3>Introduction</h3>
                    <p>A printed atlas is a multimedia product, but it cannot take advantage of all the possibilities offered by modern multimedia, such as animation. If a traditional atlas depicts a process that takes place over several years or centuries (such as the development of a city), this can usually be presented on a full page using multiple maps. If you use digital multimedia, the same can be solved with a simple animation.</p>`,
      beginnings: `<h3>The beginnings of digital multimedia</h3>
                    <p>Digital mapping began in the 1960s with the appearance of the first GIS system in Canada <i>(Canada Land Inventory)</i>. There is a short film about this on Youtube (
                    <a href="https://www.youtube.com/watch?v=eAFG6aQTwPk" target="_blank" aria-label="external link on new tab">1.part <i class="fas fa-external-link-alt fa-sm"></i></a>, 
                    <a href="https://www.youtube.com/watch?v=3kFYsOHgDSo" target="_blank" aria-label="external link on new tab">2.part <i class="fas fa-external-link-alt fa-sm"></i></a>, 
                    <a href="https://www.youtube.com/watch?v=ryWcq7Dv4jE" target="_blank" aria-label="external link on new tab">3.part <i class="fas fa-external-link-alt fa-sm"></i></a>).
                    <br>
                    The first computer animation was also made in the early 1960s: in 1962, the first animated chart was created in the U.S. AT & Bell lab, showing a change in data over time using a combination of line and scatter plots. This approximately 1-minute video was the first step in the development of dynamic representation of data.
                    <br>
                    In this video you can see another early animation.</p>`,
      maps: `<h3>Digital maps</h3>
              <p>We create digital maps with four types of software: general graphics (CorelDraw, Adobe Illustrator), GIS (ArcGIS, QGIS, Geomedia, Global Mapper, Mapinfo), cartography (OCAD) and engineering (AutoCAD). In these programs, you can find multimedia options that add color and enrich the representation of your data: all of which allow you to add images, sounds, and videos to your maps, as well as create charts, 3D models, and animations.</p>`,
      atlases: `<h3>Digital atlases with multimedia</h3>
                <p>The development of digital cartography has also led to the emergence of digital atlases with multimedia content. These atlases are an improvement on printed traditional atlases. Digital processing offers benefits not found in printed atlases: maps can be supplemented with more and better images and video excerpts. Digital atlas management has become interactive: among other things, the user can select which map they want to print or what content they want to add to the map, as most digital atlases allow you to turn selected information (terrain, hydrography, borders, etc.) on and off. An example of a digital atlas with multimedia content is the
                <a href="https://atlasderschweiz.ch/" target="_blank" aria-label="external link on new tab">Atlas of Switzerland <i class="fas fa-external-link-alt fa-sm"></i></a>.</p>`,
    },
    webcartography: {
      title: 'Multimedia in webcartography',
      maps: `<h3>Maps on the web</h3>
            <p>Today, more and more cartographic products are available via the World Wide Web in the form of digital atlases, didactic games or educational content. Research jobs also upload their developed applications and research results to the Web. In this chapter, we show some examples of the myriad uses of web cartography.</p>`,
      interactivity: `<h3>Interactivity in web atlases</h3>
                      <p>Map services available via the Web make editing maps easy. This is called interactivity: the user can decide which of the offered topics they want to display and print later if they wish. To this we must add that the user can select the elements of the base map: whether it has a topography, hydrography, road network, and so on. A good example of this is the 
                      <a href="https://apps.nationalmap.gov/viewer/" target="_blank" aria-label="external link on new tab">USGS National Map Viewer <i class="fas fa-external-link-alt fa-sm"></i></a>.</p>`,
      arcgis_online: `<h3>ArcGIS Online</h3>
                      <p>ArcGIS Online is being developed by the American company ESRI. With its applications, you can create various web maps and applications, which you can then embed in your website, for example.
                      <br>
                      With the Nearby application, you can create a map with which the user can find out what shops, parks, swimming pools, etc. can be found at a certain distance from a given point.
                      <br>
                      StoryMaps can be used to create applications that tell a story with maps and multimedia content. 
                      <br>
                      With the Dashboard app we can display various charts and analyze data. 
                      <br>
                      In addition to these, there are many other applications, each suitable for different purposes.</p>`,
      real_time: `<h3>Real time data on maps</h3>
                  <p>With the spread of web cartography, there are now maps that display data in near real-time.
                  <br>
                  The purpose of these is often to track the movement of some kind of vehicles, such as on 
                  <a href="https://www.vesselfinder.com/" target="_blank" aria-label="external link on new tab"> the website Vesselfinder <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , for ships, or 
                  <a href="https://www.flightradar24.com/47.45,19.37/6" target="_blank" aria-label="external link on new tab"> Flightradar <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , with which we can follow the movement of aircraft.
                  <br>
                  Some other examples are the 
                  <a href="https://earth.nullschool.net/" target="_blank" aria-label="external link on new tab">site that displays the Earth’s weather and ocean conditions <i class="fas fa-external-link-alt fa-sm"></i></a>
                  , which shows the data in the form of animation, and also the globe on the 
                  <a href="https://github.com/" target="_blank" aria-label="external link on new tab"> main page of GitHub <i class="fas fa-external-link-alt fa-sm"></i></a>.</p>`,
      senseable: `<h3>Senseable City</h3>
                  <p>The Senseable City Laboratory is a research initiative at the Massachusetts Institute of Technology. The lab aims to investigate and anticipate how digital technologies are changing the way people live and their implications at the urban scale. 
                  <br>
                  <br>
                  Some of their projects are: a 
                  <a href="https://senseable.mit.edu/desirable-streets/" target="_blank" aria-label="external link on new tab">map <i class="fas fa-external-link-alt fa-sm"></i></a>
                  showing the desirability of streets and comparing the most desirable route to the sortest route to a destination, an animated 
                  <a href="https://senseable.mit.edu/roboat_summer_day/" target="_blank" aria-label="external link on new tab">map <i class="fas fa-external-link-alt fa-sm"></i></a>
                  showing the movement of boats and ships on the canals of Amsterdam, a 
                  <a href="https://senseable.mit.edu/mit-world" target="_blank" aria-label="external link on new tab">map <i class="fas fa-external-link-alt fa-sm"></i></a>
                  showing the number of international students at MIT and where they came from.</p>`,
      vakeger: `<h3>Vakegér: a blind map game</h3>
                <p>There are a few interactive applications on the 
                <a href="http://lazarus.elte.hu/" target="_blank" aria-label="external link on new tab">old website of the Institute of Cartography and Geoinformatics <i class="fas fa-external-link-alt fa-sm"></i></a>
                , ELTE. One of these is the 
                <a href="http://lazarus.elte.hu/vakeger/index.php?lang=en" target="_blank" aria-label="external link on new tab">Vakegér blind map game <i class="fas fa-external-link-alt fa-sm"></i></a>
                developed by the teachers and the doctoral students of the department. It is part of a collection of three independent games: the blind map game, the Vakegér 3D, and the Vakegér on Mars. The blind map game is very easy to use: you have to log in as a player on the home page, then specify what level you want to play and, depending on the selected level, what elements the blind map should contain and in which categories (for example, settlements and/or world heritage sites) should it test your knowledge on. Then a blank map appears and you can start placing the names listed on the right.</p>`,
    },
    mapping_services: {
      title: 'Mapping services',
      mapping: `<h3>Web Mapping Services</h3>
                <p>Web mapping services are Geographic Information Systems (GIS) that provide maps over the Internet. They have brought many geographical datasets, including free ones generated by OpenStreetMap and datasets owned by HERE, Google, TomTom, and others.<p>`,
      google: `<h3>Google Maps</h3>
               <p>Google Maps is the most widely used web mapping product. It offers satellite imagery, aerial photography, street maps, 360° interactive panoramic views of streets (Street View), real-time traffic conditions, and route planning. You can also upload images and videos to the map, and make your own storymaps with the Google My Maps application.</p>`,
      osm: `<h3>OpenStreetMap</h3>
            <p>OpenStreetMap(OSM) is another well known mapping product. It is a collaborative project to create a free editable map of the world. The geodata underlying the map is considered the primary output of the project. Data is collected through crowdsourcing and it is then made available under the Open Database License.</p>`,
      other: `<h3>Other Mapping Services</h3>
              <p>There are other mapping services like Apple Maps, Bing Maps and Here WeGo. 
              <br>
              Apple Maps is the default map system of the operating systems that are developed by Apple.
              <br>
              Bing Maps are provided as a part of Microsoft's Bing suite of search engines. 
              <br>
              HERE WeGo is a web mapping and navigation service, operated by Here Technologies. Its data is also provided by HERE, which includes satellite views, traffic data, and other location services.</p>`,
    },
    modern_technologies: {
      title: 'Modern technologies',
      AR: `<h3>What is Augmented Reality?</h3>
          <p>Augmented Reality (AR) is the visual image of the real physical environment that is augmented by virtual elements. These virtual elements can be, for example, images, models, videos or even sounds.
          <br>
          AR is similar to virtual reality (VR), but while the purpose of the latter is to perceive preferably only the virtual world, in AR we supplement the real world with virtual elements, i.e. we perceive the two at the same time.
          <br>
          Usually, an app installed on a smartphone or tablet is used to experience augmented reality, but there are also smart glasses and head-up displays (HUDs).</p>`,
      marker: `<h3>Marker based AR</h3>
              <p>This kind of augmented reality requires some special visual marker, it can be a QR code, but it can also be a map or any other graphic. The application recognizes the marker with the camera on the device and it displays information that is related this marker. The AR device can also determine the position and orientation of the marker and place the content accordingly.
              <br>
              <br>
              <a href="https://elsa3dmap.com/" target="_blank" aria-label="external link on new tab">Elsa 3D Map <i class="fas fa-external-link-alt fa-sm"></i></a> 
              uses this kind of augmented reality to display the thematics on the maps or to make a map puzzle.
              <br>
              <br>
              Another example is the 
              <a href="https://the7thcontinent.seriouspoulp.com/en/resources/mobile_app" target="_blank" aria-label="external link on new tab">mobile application of the 7th Continent board game <i class="fas fa-external-link-alt fa-sm"></i></a>. 
              If you place your mobile over a specific part of the map, some virtual element will appear.</p>`,
      location: `<h3>Location based AR</h3>
                <p>Location-based augmented realities use the device’s GPS, compass, gyroscope, and accelerometer to determine the user’s location and display different virtual content depending on it. It is most often used to display nearby stores or directions for some place.
                <br>
                <br>
                There are also a lot of AR apps for viewing and identifying constellations. 
                <a href="https://play.google.com/store/apps/details?id=com.vitotechnology.StarWalk2Free&hl=en&gl=US" target="_blank" aria-label="external link on new tab"> Star Walk 2 <i class="fas fa-external-link-alt fa-sm"></i></a>
                 is also an application that has AR functionality among many other things. When we use this, we project the name and visual of the constellations on the image of the real sky, similarly to a star map.</p>`,
      sandbox: `<h3>Augmented Reality Sandbox</h3>
                <p>With <a href="https://arsandbox.ucdavis.edu/" target="_blank" aria-label="external link on new tab">AR Sandbox <i class="fas fa-external-link-alt fa-sm"></i></a>
                you can create topography models by shaping sand.
                <br>
                It uses a projector and a motionsensing input device (a Kinect 3D camera) mounted above a box of sand. The camera detects the distance to the sand below, and an elevation model with contour lines and a color map is cast from the overhead projector onto the surface of the sand. When an object (for example, a hand) is sensed at a particular height above the sand, virtual rain appears on the surface below.</p>`,
    },
    contact: {
      title: 'Contact',
      address: `Address: 1117 Budapest, Pázmány Péter sétány 1/A<br>
                Postal address: 1518 Budapest, Pf. 32.<br>
                Direct telephone number: 3722975<br>
                Fax: 3722951<br>
                Email: terktud{'@'}ludens.elte.hu<br>`,
    },
    attributions: {
      title: 'Attributions',
      content: `<p>
                  <b>Source of the modern technologies topic:</b> 
                  <br>
                  Yonov, Nikola. (2019). School Atlas with Augmented Reality.
                  <br>
                  Proceedings of the ICA. 2. 1-6. 10.5194/ica-proc-2-150-2019.
                </p>
                <p>
                  <b>Primary sources of the images:</b>
                  <br>
                  Wikimedia Commons, David Rumsey's Map Collection, school atlases
                  <br>
                  Other images, videos about some of the softwares were made by me.
                </p>                
                <p>
                  The background images were made with 
                  <a href="https://alvarcarto.com/phone-background/" target="_blank" aria-label="external link on new tab">Alvar Carto's <i class="fas fa-external-link-alt fa-sm"></i></a>
                  free background maker.
                </p>                
                <p>
                  choropleth maps 1.image: by Toby Hudson
                  <br>
                  <a href="https://creativecommons.org/licenses/by-sa/3.0/au/" target="_blank" aria-label="external link on new tab">CC BY-SA 3.0 AU <i class="fas fa-external-link-alt fa-sm"></i></a>
                </p>
                <p>
                  choropleth maps 2.image: by Derek Montaño
                  <br>
                  choropleth maps 4.image: by ThePromenader
                  <br>
                  choropleth maps 5.image: by Moyogo
                  <br>
                  <a href="https://creativecommons.org/licenses/by-sa/3.0/" target="_blank" aria-label="external link on new tab">CC BY-SA 3.0 <i class="fas fa-external-link-alt fa-sm"></i></a>
                </p>
                <p>
                  representation with points 1.image: by Shayna Fever
                  <br>
                  representation with points 2.image: by Kelly Ziegenfuss
                  <br>    
                  <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" aria-label="external link on new tab">CC BY-SA 4.0 <i class="fas fa-external-link-alt fa-sm"></i></a>
                </p>`,
    },
  }
}
