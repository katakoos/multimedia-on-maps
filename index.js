import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import routes from './routes.js';
import Index from './Index.vue';
import { createI18n } from 'vue-i18n';
import { messages } from './translate.js';

import '@glidejs/glide/dist/css/glide.core.min.css';
import '@glidejs/glide/dist/css/glide.theme.min.css';
import 'leaflet/dist/leaflet.css';

createApp(Index)
  .use(createI18n({
    locale: 'hu',
    fallbackLocale: 'en',
    messages,
    warnHtmlInMessage: 'off',
  }))
  .use(createRouter({
    history: createWebHistory(),/*TODO: '/~kookataa/szakdoga/dist/'*/
    routes,
    scrollBehavior () {
      return { left: 0, top: 0 };
    },
  }))
  .mount('#index')
;
