import Home from './pages/Home.vue';
import Contact from './pages/Contact.vue';
import Content from './pages/Content.vue';
import data from './pages/data.vue';
import first_thematic_maps from './pages/first_thematic_maps.vue';
import modern_thematic_maps from './pages/modern_thematic_maps.vue';
import traditional_multimedia from './pages/traditional_multimedia.vue';
import digital_multimedia from './pages/digital_multimedia.vue';
import web_cartography from './pages/web_cartography.vue';
import mapping_services from './pages/mapping_services.vue';
import modern_technologies from './pages/modern_technologies.vue';
import Attributions from './pages/Attributions.vue';

export default [
  {
    name: 'home',
    path: '/',
    component: Home,
  },
  {
    name: 'contact',
    path: '/contact',
    component: Contact,
  },
  {
    name: 'content',
    path: '/content',
    component: Content,
    children: [
      {
        name: 'data',
        path: '',
        component: data,
      },
      {
        name: 'first_thematic_maps',
        path: 'first_thematic_maps',
        component: first_thematic_maps,
      },
      {
        name: 'modern_thematic_maps',
        path: 'modern_thematic_maps',
        component: modern_thematic_maps,
      },
      {
        name: 'traditional_multimedia',
        path: 'traditional_multimedia',
        component: traditional_multimedia,
      },
      {
        name: 'digital_multimedia',
        path: 'digital_multimedia',
        component: digital_multimedia,
      },
      {
        name: 'web_cartography',
        path: 'web_cartography',
        component: web_cartography,
      },
      {
        name: 'mapping_services',
        path: 'mapping_services',
        component: mapping_services,
      },  
      {
        name: 'modern_technologies',
        path: 'modern_technologies',
        component: modern_technologies,
      }, 
    ],
  },
  {
    name: 'attributions',
    path: '/attributions',
    component: Attributions,
  },
];
