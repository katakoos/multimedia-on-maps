const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env, options) => ({
  entry: [
    path.resolve(__dirname, 'index.js'),
    path.resolve(__dirname, 'index.styl'),
  ],
  output: {
    filename: '[name].[contenthash:8].js',
    chunkFilename: '[name].[chunkhash:8].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
      },
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)$/,
        include: /fonts/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[contenthash:8].[ext]',
          },
        }
      },
      {
        test: /\.(png|jpg|jpeg|svg|mp4)$/,
        include: /images/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'images/[name].[contenthash:8].[ext]',
          },
        }
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: false,
            },
          },
        ]
      },
      {
        test: /\.styl$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
          'stylus-loader',
        ]
      },
    ],
  },
  // resolve: { extensions: ['.js', '.json', '.vue', '.css' ] },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash:8].css',
    }),
  ],
  devServer: {
    port: process.env.PORT,
    host: '0.0.0.0',
    hot: true,
    disableHostCheck: true,
    historyApiFallback: true,
  },
});
